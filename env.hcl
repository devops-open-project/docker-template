consul {
  address = "https://consul.mick8s.local"
  token_file = "/app/consul.token"
}

vault {
  address = "https://vault.mick8s.local"
  vault_agent_token_file = "/app/vault.token"
  default_lease_duration = "5s"
  renew_token = false
}


log_level = "info"

template {
  source = "/app/env.ctmpl"
  destination = "/app/config/.env"
}

exec {
  # This is the command to exec as a child process. There can be only one
  # command per Consul Template process.
  # Please see the Commands section in the README for more.
  command = "/app/application -config /app/config/.env"
}
