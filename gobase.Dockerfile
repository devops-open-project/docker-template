# syntax=docker/dockerfile:1

# Build the application from source
FROM golang:1.21rc3-bookworm AS build-stage
WORKDIR /app
COPY * ./
RUN apt update && apt install -y unzip wget
RUN wget https://releases.hashicorp.com/consul-template/0.32.0/consul-template_0.32.0_linux_amd64.zip && unzip consul-template_0.32.0_linux_amd64.zip -d /app/

# Run the tests in the container
FROM build-stage AS run-test-stage
RUN go test -v ./...
# Deploy the application binary into a lean image
#FROM gcr.io/distroless/base-debian11 AS build-release-stage
FROM golang:1.21rc3-bookworm AS build-release-stage
WORKDIR /app
RUN chmod -R 755 /app && mkdir /app/config && chmod 777 /app/config
COPY --from=build-stage /app/consul-template /usr/local/bin/consul-template
COPY --from=build-stage /app/env.hcl /app/env.hcl
ADD consul.token /app/consul.token
ADD vault.token /app/vault.token